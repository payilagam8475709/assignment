class Kid extends Mother{
	
	String name = "Suman";
	
	public static void main(String [] args) {
		Kid kid = new Kid();
		kid.study();
	}
	
	public void work()
	{
		System.out.println("Animation");
		System.out.println(name + " "+super.name);
	}
	
	public void study()
	{
		System.out.println("Studing....");
		work();
	}

}



class Mother extends Grandma{  
	
	String name = "Stella";
	
	public void work()
	{
		super.work();
		System.out.println(name +" " +super.name);
	}
	
}


